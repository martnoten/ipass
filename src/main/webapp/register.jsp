<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Registreren</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
</head>
<%
	String userName = "";

	Object msgs = request.getAttribute("msgs");

	if (msgs != null) {
		out.println(msgs);
	}

	if (request.getCookies() != null) {
		for (Cookie c : request.getCookies()) {
			if (c.getName().equals("cUsername")) {
				userName = c.getValue();
				break;
			}
		}
	}
%>
<body>
	<div class="row">
		<jsp:include page="/menu.jsp" />

		<form action="/RegisterServlet.do" method="post">
			<h4>Registreren</h4>

			<label>Gebruikersnaam: </label> <input type="text" name="username"
				class="input-group-field" /> <label>Wachtwoord: </label> <input
				type="password" name="password" /> <label>Wachtwoord(Controle):
			</label> <input type="password" name="password2X" /> <label>Email: </label>
			<input type="text" name="email" class="input-group-field" /> <label>Volledige
				naam: </label> <input type="text" name="naam" class="input-group-field" />

			<input class="button" type="submit" value="Registreren" />
		</form>
	</div>
</body>
</html>
