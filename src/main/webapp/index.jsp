<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>inloggen</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
</head>
<%
	String userName = "";

	Object msgs = request.getAttribute("msgs");

	if (msgs != null) {
		out.println(msgs);
	}

	if (request.getCookies() != null) {
		for (Cookie c : request.getCookies()) {
			if (c.getName().equals("cUsername")) {
				userName = c.getValue();
				break;
			}
		}
	}
%>
<body>

	<div class="row">
		<jsp:include page="menu.jsp" />

		<form action="/LoginServlet.do" method="post">
			<h4>Hier kun je inloggen</h4>

			<label>Gebruikersnaam: </label> <input type="text" name="username"
				class="input-group-field" /> <label>Wachtwoord: </label> <input
				type="password" name="password" /> <input class="button"
				type="submit" value="Login!" />
		</form>

		<hr>

		<a href="register.jsp" class="secondary button"> Registreren</a>

	</div>
<script src="js/vendor/jquery.js"></script>
<!-- this will include every plugin and utility required by Foundation -->
<script src="js/vendor/foundation.min.js"></script>
</body>
</html>
