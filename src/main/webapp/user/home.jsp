<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Homepagina</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
    prefix="fn" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
</head>
<body>

	<div class="row">

		<jsp:include page="menu.jsp" />

		<div id="account">

			<div id="messagebox">

				<%@ page import="ipass.model.User"%>
				<%@ page import="ipass.services.ServiceProvider"%>
				<%@ page import="ipass.model.Genre"%>
				<%@ page import="ipass.model.Artiest"%>
				<%@ page import="ipass.model.Bibliotheek"%>
				<% request.setAttribute("service", ServiceProvider.getLibService()); %>
					<% request.setAttribute("artiestLijst", ServiceProvider.getLibService().getArtiestList()); %>
					<% request.setAttribute("albumLijst", ServiceProvider.getLibService().getAlbumList()); %>
					<% request.setAttribute("genreLijst", ServiceProvider.getLibService().getGenreList()); %>
					<% request.setAttribute("bibliotheekLijst", ServiceProvider.getLibService().getBibliotheekList()); %>
					
					<%
					Object msgs = request.getAttribute("msgs");
					if (msgs != null) {
						out.println(msgs);
					}

					Object naam = request.getAttribute("naam");

					// Lees gebruiker Object
					Object loggedUser = request.getSession().getAttribute("loggedUser");
					User u = (User) loggedUser;

					if (loggedUser == null) {
						RequestDispatcher rd = null;
						rd = request.getRequestDispatcher("myaccount.jsp");
						rd.forward(request, response);
					}
				%>

			</div>


			<div class="row">
				<div class="small-8 medium-6 large-5 small-centered columns">

					<div class="header panel">
						<div class="sign">
							<br>
							<br>
							<h1>
								Welkom
								<%=u.getName()%>!
							</h1>
						</div>
						<p>
						<div class="alert-box warning">
							<h5> <c:out value="${fn:length(albumLijst)}" /> albums
      						<a href="albumOverzicht.jsp" class="close"> Bekijk ze </a>
      						</h5>
    					</div>

						<div class="alert-box warning">
							<h5><c:out value="${fn:length(artiestLijst)}" /> artiesten
      						<a href="artiestenOverzicht.jsp" class="close"> Bekijk ze </a>
      						</h5>
    					</div>
    					
    					<div class="alert-box warning">
							<h5><c:out value="${fn:length(genreLijst)}" /> Genre's
      						<a href="genreOverzicht.jsp" class="close"> Bekijk ze </a>
      						</h5>
    					</div>
    					
    					 <div class="alert-box warning">
							<h5><c:out value="${fn:length(bibliotheekLijst)}" /> Bibliotheken
      						<a href="bibliotheken.jsp" class="close"> Bekijk ze </a>
      						</h5>
    					</div>
    					
						
					</div>

				</div>
			</div>

		</div>

	</div>

</body>
</html>