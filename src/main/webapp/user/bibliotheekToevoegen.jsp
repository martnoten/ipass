<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Genre Toevoegen</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">
	
	<jsp:include page="menu.jsp" />
			
	<div id="account">

		<div id="messagebox">
			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<%@ page import="ipass.model.Bibliotheek"%>
			<% request.setAttribute("service", ServiceProvider.getLibService()); %>
			<% request.setAttribute("artiestLijst", ServiceProvider.getLibService().getArtiestList()); %>
			<% request.setAttribute("genreLijst", ServiceProvider.getLibService().getGenreList()); %>


			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}
				
				Object naam = request.getAttribute("naam");

				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;
				
				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		
		<h3>
		</h3>
		<br>
		<form action="/BibliotheekToevoegenServlet.do" method="post">
			<h5> Hier kun je een Bibliotheek toevoegen:</h5>
			
			<label>Naam van de Bibliotheek: </label> 
				<input type="text" name="bibliotheekNaam" class="input-group-field" />  <br>
							
			<br><br>
			<input type="submit" class="success button expanded" value="Toevoegen" />
		</form>
		<br><br>

</div>
</div>
</body>
</html>