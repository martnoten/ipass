<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Bibliotheek Verwijderen</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">
	
	<jsp:include page="menu.jsp" />
			
	<div id="account">

		<div id="messagebox">
			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<% request.setAttribute("bibliotheekLijst", ServiceProvider.getLibService().getBibliotheekList()); %>

			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}

				Object naam = request.getAttribute("naam");

				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;

				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		
		<br>
		<br>
				<form action="/BibliotheekVerwijderenServlet.do" method="post">
			<h5> Welke bibliotheek wil je graag verwijderen?</h5>
			
			<label>Bibliotheek naam: </label> 
				<select name="naam">
				  	<c:forEach var="bieb" items="${bibliotheekLijst}">
				  		<option value="${bieb.naam}"> <c:out value="${bieb.naam}" /> </option>	
				  	</c:forEach>
				</select>
			<br><br>
			<input class="alert button expanded" type="submit" value="Verwijderen" />
		</form>
	</div>

</div>

</body>
</html>