<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Genre Overzicht</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">
	
	<jsp:include page="menu.jsp" />

		<div id="messagebox">

			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<%@ page import="ipass.model.Bibliotheek"%>
			<% request.setAttribute("service", ServiceProvider.getLibService()); %>
			<% request.setAttribute("artiestLijst", ServiceProvider.getLibService().getArtiestList()); %>
			<% request.setAttribute("albumLijst", ServiceProvider.getLibService().getAlbumList()); %>
			<% request.setAttribute("bibliotheekLijst", ServiceProvider.getLibService().getBibliotheekList()); %>
			<% request.setAttribute("genreLijst", ServiceProvider.getLibService().getGenreList()); %>
			
			
			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}

				Object naam = request.getAttribute("naam");

				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;

				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		
		<section>
		<h3>
			Uw genre's
		</h3>
		<h6> Dit zijn alle genre's die bij ons bekend zijn: </h6>
		<ol>

			<div class="column row">
				<u>Genre naam</u>
			</div>

			<c:forEach var="genre" items="${genreLijst}">
				<div class="column row"> ${genre.naam} </div>
			</c:forEach>	
		</ol>
		</section>
</div>

</body>
</html>