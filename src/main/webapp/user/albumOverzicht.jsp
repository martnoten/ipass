<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Album Overzicht</title>
<script src="https://code.jquery.com/jquery-2.2.3.js"></script>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">

		<jsp:include page="menu.jsp" />

		<div id="account">

			<div id="messagebox">

				<%@ page import="ipass.services.ServiceProvider"%>
				<%@ page import="ipass.model.User"%>
				<%@ page import="ipass.model.Bibliotheek"%>


				<%
					Object msgs = request.getAttribute("msgs");
					if (msgs != null) {
						out.println(msgs);
					}

					Object naam = request.getAttribute("naam");

					// Lees gebruiker Object
					Object loggedUser = request.getSession().getAttribute("loggedUser");
					User u = (User) loggedUser;

					request.setAttribute("albumLijst", ServiceProvider.getLibService().getAlbumList());
				%>

			</div>

			<h3>Uw albums</h3>
			<br>
			<h6>Dit zijn alle albums die bij ons bekend zijn:</h6>
			<ol>
				<div class="medium-4 columns">
					<u>Album naam</u>
				</div>
				<div class="medium-4 columns">
					<u>Artiest</u>
				</div>
				<div class="medium-2 columns">
					<u>Type</u>
				</div>
				<div class="medium-2 columns end"></div>

				<c:forEach var="album" items="${albumLijst}">
					<div class="medium-4 columns">${album.naam}</div>
					<div class="medium-4 columns">${album.artiest}</div>
					<div class="medium-2 columns">${album.type}</div>
					<div class="medium-2 columns">
					<button class="tiny button" type="button">${album.naam}</button>
					</div>
				</c:forEach>
			</ol>
		</div>

	</div>
	<script>
		$(document)
				.ready(
						function() {
							$('button').on('click', function() {
								// get the button that was clicked
								var buttonClicked = $(this).html();
								console.log(buttonClicked);
								sessionStorage.setItem("album", buttonClicked);

								window.location.href = 'albumInformatie.jsp';

							});
						});
	</script>
</body>
</html>