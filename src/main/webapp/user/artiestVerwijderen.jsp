<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Artiest Verwijderen</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">
	
	<jsp:include page="menu.jsp" />
			
	<div id="account">

		<div id="messagebox">
			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<% request.setAttribute("artiestLijst", ServiceProvider.getLibService().getArtiestList()); %>

			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}

				Object naam = request.getAttribute("naam");

				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;

				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		
		<br>
		<br>
				<form action="/ArtiestVerwijderenServlet.do" method="post">
			<h5> Welke artiest wil je graag verwijderen?</h5>
			
			<label>Artiest: </label> 
				<select name="naam">
				  	<c:forEach var="artiest" items="${artiestLijst}">
				  		<option value="${artiest.naam}"> <c:out value="${artiest.naam}" /> </option>	
				  	</c:forEach>
				</select>
			<br><br>
			<input class="alert button expanded" type="submit" value="Verwijderen" />
		</form>
	</div>

</div>

</body>
</html>