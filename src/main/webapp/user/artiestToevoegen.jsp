<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Artiest Toevoegen</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">
	
	<jsp:include page="menu.jsp" />
			
	<div id="account">

		<div id="messagebox">
			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<% request.setAttribute("artiestLijst", ServiceProvider.getLibService().getArtiestList()); %>

			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}

				Object naam = request.getAttribute("naam");

				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;

				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		
		<br>
		<form action="/ArtiestToevoegenServlet.do" method="post">
			<h5> Hier kun je een artiest toevoegen:</h5>
			
			<label>Artiest naam: </label> 
				<input type="text" name="naam" class="input-group-field" /> 
				
			<label>Instrument: </label>
				<input type="text" name="instrument" class="input-group-field" /> 
			
			<br><br>
			<input class="success button expanded" type="submit" value="Toevoegen" />
		</form>
		<br><br>
		<hr><br>
				<form action="/ArtiestVerwijderenServlet.do" method="post">
			<h5> Hier kun je een artiest verwijderen op naam:</h5>
			
			<label>Artiest naam: </label> 
				<select name="naam">
				  	<c:forEach var="artiest" items="${artiestLijst}">
				  		<option value="${artiest.naam}"> <c:out value="${artiest.naam}" /> </option>	
				  	</c:forEach>
				</select>
			<br><br>
			<input class="alert button expanded" type="submit" value="Verwijderen" />
		</form>
	</div>

</div>

</body>
</html>