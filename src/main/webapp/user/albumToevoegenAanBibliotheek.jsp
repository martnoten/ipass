<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Album Toevoegen Aan Bibliotheek</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">
	
	<jsp:include page="menu.jsp" />

		<div id="messagebox">

			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<%@ page import="ipass.model.Bibliotheek"%>
			<% request.setAttribute("service", ServiceProvider.getLibService()); %>
			<% request.setAttribute("artiestLijst", ServiceProvider.getLibService().getArtiestList()); %>
			<% request.setAttribute("albumLijst", ServiceProvider.getLibService().getAlbumList()); %>
			<% request.setAttribute("bibliotheekLijst", ServiceProvider.getLibService().getBibliotheekList()); %>
			
			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}

				Object naam = request.getAttribute("naam");

				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;

				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		
		<h3>
			Een album toevoegen aan een bibliotheek
		</h3>

		<br><br><br><hr><br><br>
		<section>
		<form action="/AlbumToevoegenAanBibliotheekServlet.do" method="post">
				<label>Album: </label>
				<select name="albumNaam">
				  	<c:forEach var="album" items="${albumLijst}">
				  		<option value="${album.naam}"> <c:out value="${album.naam}" /> </option>	
				  	</c:forEach>
				</select>
				
				<label>Aan bibliotheek: </label>
				<select name="bibliotheekNaam">
				  	<c:forEach var="bieb" items="${bibliotheekLijst}">
				  		<option value="${bieb.naam}"> <c:out value="${bieb.naam}" /> </option>	
				  	</c:forEach>
				</select>
				
			<input type="submit" class="alert button expanded" value="Album toevoegen aan bibliotheek" />
		</form>
		</section>

</div>

</body>
</html>