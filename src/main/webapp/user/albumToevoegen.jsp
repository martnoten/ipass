<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Album toevoegen</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">
	
	<jsp:include page="menu.jsp" />
			
	<div id="account">

		<div id="messagebox">
			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<%@ page import="ipass.model.Bibliotheek"%>
			<% request.setAttribute("service", ServiceProvider.getLibService()); %>
			<% request.setAttribute("artiestLijst", ServiceProvider.getLibService().getArtiestList()); %>
			<% request.setAttribute("genreLijst", ServiceProvider.getLibService().getGenreList()); %>
			<% request.setAttribute("bibliotheekLijst", ServiceProvider.getLibService().getBibliotheekList()); %>

			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}
				
				Object naam = request.getAttribute("naam");

				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;
				
				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		
		<h3>
		</h3>
		<br>
		
		<form action="/AlbumToevoegenServlet.do" method="post">
			<h5> Hier kun je een album toevoegen:</h5>
			
			<label>Naam van het album: *</label> 
				<input type="text" name="albumNaam" class="input-group-field" />  <br>
					
			<label>Naam van de artiest: *</label>
				<select name="artiestNaam">
				  	<c:forEach var="artiest" items="${artiestLijst}">
				  		<option value="${artiest.naam}"> <c:out value="${artiest.naam}" /> </option>	
				  	</c:forEach>
				</select>
				
			<label>Waardering?: *)</label>
				<input type="number" name="waardering" class="input-group-field" /> 
				
			<label>Aan bibliotheek: </label>
				<select name="bibliotheekNaam">
				  	<c:forEach var="bieb" items="${bibliotheekLijst}">
				  		<option value="${bieb.naam}"> <c:out value="${bieb.naam}" /> </option>	
				  	</c:forEach>
				</select>
				
			<label>Genre: *</label>
				<select name="genre">
				  	<c:forEach var="genre" items="${genreLijst}">
				  		<option value="${genre.naam}"> <c:out value="${genre.naam}" /> </option>	
				  	</c:forEach>
				</select>
				
			<label>Naam van de uitvoerende artiest: </label>
				<input type="text" name="uitvoerende_artiest" class="input-group-field" /> 

			<label>Type van het album: *</label>
				<input type="text" name="type" class="input-group-field" /> 
			
			<label>Link naar de cover(Wordt automatisch ingevuld wanneer leeg)</label>
				<input type="text" name="coverLink" class="input-group-field" /> 
				
			<br><br>
			<input type="submit" class="success button expanded" value="Toevoegen" />
		</form>
		
		
		<a href="albumOverzicht.jsp" class="button expanded">Album overzicht</a>
		<form action="albumVerwijderen.jsp" method="post">
			<input type="submit" class="alert button expanded" value="Album Verwijderen" />
		</form>
</div>
</div>
</body>
</html>