<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CDLIB | Album Informatie</title>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>

	<div class="row">

		<jsp:include page="menu.jsp" />

		<div id="messagebox">

			<%@ page import="ipass.services.ServiceProvider"%>
			<%@ page import="ipass.model.User"%>
			<%
				// Lees gebruiker Object
				Object loggedUser = request.getSession().getAttribute("loggedUser");
				User u = (User) loggedUser;

				if (loggedUser == null) {
					RequestDispatcher rd = null;
					rd = request.getRequestDispatcher("myaccount.jsp");
					rd.forward(request, response);
				}
			%>

		</div>
		<br>

		<h5 class="subheader">
			<table id="albumGegevens">
			</table>
		</h5>



	</div>
	<script>
		$(document).ready(
				function() {
					console.log("Ik ga zoeken naar het album..");
					var album = sessionStorage.getItem("album");
					console.log("Gevonden: " + album);

					$.get("../restservices/getAlbumInfo/" + album, function(a) {
						console.log(a.albumNaam);
						$("#albumGegevens").append(
								"<img src=\""  + a.coverLink +  "\" class=\"thumbnail float-right\">"
										+ "<br>" + a.albumNaam + "<br>"
										+ a.artiestNaam + "<br>" + a.waardering
										+ " Sterren <br>" + a.type + "<br>"
										+ a.genre + "<br>")
					});

				});
	</script>
</body>
</html>