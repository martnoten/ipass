<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="ipass.model.User"%>
<%@ page import="ipass.model.Album"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="ipass.services.ServiceProvider"%>

<%
	request.setAttribute("service", ServiceProvider.getLibService());
%>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Menu</title>
<link rel="stylesheet" href="../css/foundation.css">
<link rel="stylesheet" href="../css/app.css">
</head>
<body>


	<!-- menu zonder ingelogde user -->
	<c:if test="${loggedUser == null}">

		<div class="top-bar">
			<div class="top-bar-title">
				<span data-responsive-toggle="responsive-menu"
					data-hide-for="medium">
					<button class="menu-icon dark" type="button" data-toggle></button>
				</span> <strong>Site Title</strong>
			</div>
			<div id="responsive-menu">
				<div class="top-bar-left">
					<ul class="dropdown menu" data-dropdown-menu>
						<li><a href="/user/home.jsp">Home</a></li>
						<li><a href="/user/genreToevoegen.jsp">Genre's</a></li>
						<li><a href="/user/artiestToevoegen.jsp">Artiesten</a></li>
						<li><a href="/user/albumToevoegen.jsp">Albums</a></li>
						<li><a href="/user/bibliotheken.jsp">Bibliotheken</a></li>
					</ul>
				</div>
				<div class="top-bar-right">
					<ul class="menu">
						<li><input type="search" placeholder="Search"></li>
						<li><button type="button" class="button">Search</button></li>
					</ul>
				</div>
			</div>
		</div>
	</c:if>

	<!-- menu met ingelogde user -->
	<c:if test="${loggedUser != null}">
		<!-- Medium-Up Navigation -->
		<nav class="top-bar" id="nav-menu">

			<div class="logo-wrapper hide-for-small-only">
				<div class="logo">
					<img
						src="https://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2015/2/20/1424454392161/Library-books-009.jpg">
				</div>
			</div>

			<!-- Left Nav Section -->
			<div class="top-bar-left">
				<ul class="vertical medium-horizontal menu">
					<li><a href="/user/home.jsp">Home</a></li>
					<li><a href="/user/bibliotheken.jsp">Bibliotheken</a></li>
					<li class="has-submenu">
						<ul class="vertical medium-horizontal dropdown menu"
							data-dropdown-menu>
							<li class="has-submenu"><a href="#">Toevoegen</a>
								<ul class="submenu menu vertical medium-horizontal" data-submenu>
									<li><a href="/user/genreToevoegen.jsp">Genre</a></li><br>
									<li><a href="/user/artiestToevoegen.jsp">Artiest</a></li><br>
									<li><a href="/user/albumToevoegen.jsp">Album</a></li><br>
									<li><a href="/user/bibliotheekToevoegen.jsp">Bibliotheek</a></li><br>
									<li><a href="/user/albumToevoegenAanBibliotheek.jsp">Album toevoegen aan bibliotheek</a></li>
								</ul></li>
						</ul>
					</li>

					<li class="has-submenu">
						<ul class="vertical medium-horizontal dropdown menu"
							data-dropdown-menu>
							<li class="has-submenu"><a href="#">Overzicht</a>
								<ul class="submenu menu vertical medium-horizontal" data-submenu>
									<li><a href="/user/genreOverzicht.jsp">Genre's</a></li><br>
									<li><a href="/user/artiestenOverzicht.jsp">Artiesten</a></li><br>
									<li><a href="/user/albumOverzicht.jsp">Albums</a></li><br>
								</ul></li>
						</ul>
					</li>

															<li><a href="/user/zoeken.jsp">Zoeken</a></li>

				</ul>
			</div>

			<!-- Right Nav Section -->
			<div class="top-bar-right">			
				<ul class="vertical medium-horizontal dropdown menu"
					data-dropdown-menu>
					<li style="float: right"
										
					><form action="/LogoutServlet.do"
							method="post">
							<input class="button" type="submit" value="Logout" />
					</form></li>
					
								<li class="has-submenu"><a href="verwijderen.jsp">Verwijderen</a>
								<ul class="submenu menu vertical medium-horizontal" data-submenu>
									<li><a href="/user/genreVerwijderen.jsp">Genre's</a></li><br>
									<li><a href="/user/artiestVerwijderen.jsp">Artiesten</a></li><br>
									<li><a href="/user/albumVerwijderen.jsp">Albums</a></li><br>
									<li><a href="/user/bibliotheekVerwijderen.jsp">Bibliotheek</a></li><br>
								</ul></li>
								


					
					</ul>
			</div>


		</nav>
	</c:if>

	<script src="../js/vendor/jquery.js"></script>
	<script src="../js/vendor/foundation.min.js"></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>