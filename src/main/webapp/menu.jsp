<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="ipass.model.User"%>
<%@ page import="ipass.model.Album"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="ipass.services.ServiceProvider"%>

<%
	request.setAttribute("service", ServiceProvider.getLibService());
%>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Menu</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
</head>
<body>

	<!-- Medium-Up Navigation -->
	<nav class="top-bar" id="nav-menu">

		<div class="logo-wrapper hide-for-small-only">
			<div class="logo">
				<img src="https://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2015/2/20/1424454392161/Library-books-009.jpg">
			</div>
		</div>

		<!-- Left Nav Section -->
		<div class="top-bar-left">
			<ul class="vertical medium-horizontal menu">
				<li><a href="/index.jsp">Home</a></li>
				<li><a href="/register.jsp">Contact</a></li>
			</ul>
		</div>

		<!-- Right Nav Section -->
		<div class="top-bar-right">
			<ul class="vertical medium-horizontal dropdown menu"
				data-dropdown-menu>
				<li style="float: right"><form action="/index.jsp"
						method="post">
						<input class="button" type="submit" value="Inloggen" />
					</form></li>
										
      <li style="float: right"><form action="/register.jsp"
						method="post">
						<input class="button" type="submit" value="Registreren" />
						</form></li>
				
    </ul>
  </div>

</nav>

<!-- 	<!-- menu zonder ingelogde user --> 
<%-- 	<c:if test="${loggedUser == null}"> --%>

<!-- 		<div class="top-bar"> -->
<!-- 			<div class="top-bar-title"> -->
<!-- 				<span data-responsive-toggle="responsive-menu" -->
<!-- 					data-hide-for="medium"> -->
<!-- 				</span> <strong>CDLib</strong> -->
<!-- 			</div> -->
<!-- 			<div id="responsive-menu"> -->
<!-- 				<div class="top-bar-left"> -->
<!-- 					<ul class="dropdown menu" data-dropdown-menu> -->
<!-- 						<li><a href="/index.jsp">Inloggen</a></li> -->
<!-- 						<li><a href="/register.jsp">Registreren</a></li> -->
<!-- 					</ul> -->
<!-- 				</div> -->
<!-- 				<div class="top-bar-right"> -->
<!-- 					<ul class="menu"> -->
<!-- 						<li style="float: right"><form action="/LogoutServlet.do" -->
<!-- 								method="post"> -->
<!-- 						<input class="button" type="submit" value="Logout" /> -->
<!-- 						</form></li>	 -->
<!-- 					</ul> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
<%-- 	</c:if> --%>

	<!-- menu met ingelogde user -->
	<c:if test="${loggedUser != null}">
		<div class="top-bar">
			<div class="top-bar-title">
				<span data-responsive-toggle="responsive-menu"
					data-hide-for="medium">
				</span> <strong>CDLib</strong>
			</div>
			<div id="responsive-menu">
				<div class="top-bar-left">
					<ul class="dropdown menu" data-dropdown-menu>
						<li><a href="/user/home.jsp">Home</a></li>
					</ul>
				</div>
				<div class="top-bar-right">
					<ul class="menu">
						<li style="float: right"><form action="/LogoutServlet.do"
								method="post">
						<input class="button" type="submit" value="Logout" />
						</form></li>	
					</ul>
				</div>
			</div>
		</div>
	</c:if>
	
	<script src="js/vendor/jquery.js"></script>
	<script src="js/vendor/foundation.min.js"></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>