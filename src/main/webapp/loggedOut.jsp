<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="ipass.model.User"%>
<%@ page import="ipass.model.Album"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="ipass.services.ServiceProvider"%>

<%
	request.setAttribute("service", ServiceProvider.getLibService());
%>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Uitloggen</title>
<link rel="stylesheet" href="../css/foundation.css">
<link rel="stylesheet" href="../css/app.css">
</head>
<body>
		<h1>Uitloggen?</h1>

			<div id="messagebox">
				<%
					Object loggedUser = request.getSession().getAttribute("loggedUser");
					if (loggedUser == null) {
						out.println("Je bent succesvol uitgelogd");
						%> <br> <a href="index.jsp">Terug naar inloggen</a> <%
					} else {
						out.println("Je bent toch nog steeds ingelogd");
						%> <a href="myaccount.jsp">Terug naar MyAccount</a> <%
					}
					Object msgs = request.getAttribute("msgs");
					Object msgs2 = request.getAttribute("msgs2");

					if (msgs != null) {
						out.println(msgs);
					}

					if (msgs2 != null) {
						out.println(msgs2);
					}
				%>
			</div>
</body>
</html>