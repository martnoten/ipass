<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Welkom</title>
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="row">

		<jsp:include page="/menu.jsp" />
		<div id="messagebox">
			<%
				Object msgs = request.getAttribute("msgs");
				if (msgs != null) {
					out.println(msgs);
				}
			%>
		</div>
		<h3>Succesvol geregistreerd!</h3>

		<%
			// Lees gebruiker Object
			Object usn = request.getAttribute("username");
			Object pass = request.getAttribute("password");
			Object email = request.getAttribute("email");
			Object naam = request.getAttribute("naam");
		%>

		Gebruikersnaam:
		<%=usn%>
		<br> Wachtwoord:
		<%=pass%>
		<br> Email:
		<%=email%>
		<br> Volledige Naam:
		<%=naam%>
		<br> <br> <a href="index.jsp">Keer terug naar het
			inloggen!</a>
	</div>

</body>
</html>