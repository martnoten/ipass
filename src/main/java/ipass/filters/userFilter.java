package ipass.filters;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class userFilter implements Filter {
	public void init(FilterConfig arg0) throws ServletException {
		/* Filter is being placed into service, do nothing. */ }

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		if (((HttpServletRequest) req).getSession().getAttribute("loggedUser") == null) {
//			req.getRequestDispatcher("../").forward(req, resp);
			HttpServletResponse httpResponse = (HttpServletResponse) resp;
			httpResponse.sendRedirect("/");
			System.out.println("userFilter redirected naar home");
		} else {
			chain.doFilter(req, resp);
		}
	}

	public void destroy() {
		/* Filter is being taken out of service, do nothing. */
	}
}