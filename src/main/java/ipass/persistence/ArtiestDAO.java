package ipass.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ipass.model.Artiest;

public class ArtiestDAO extends BaseDAO{
	
	// selectArtiest functie
	private ArrayList<Artiest> selectArtiest(String query) {
		ArrayList<Artiest> results = new ArrayList<Artiest>();

		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				String naam = dbResultSet.getString("naam");
				String instrument = dbResultSet.getString("instrument");


				results.add(new Artiest(naam, instrument));
				System.out.println("selectArtiest Result added: " + naam);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	// Save statement (Save Artiest in Database)
	public void Save(Artiest artiest) {
		String query = "INSERT INTO artiesten(naam, instrument) VALUES ('" + artiest.getNaam() + "', '" + artiest.getInstrument() + "');";
		System.out.println(query);
		
		try (Connection con = getConnection()) {
			Statement stmt = con.createStatement();
			if (stmt.executeUpdate(query) == 1) { // 1 row updated!
				// Succesvol geupdate
				System.out.println("Succesvol gesaved");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
	}

	// Find all()
	public ArrayList<Artiest> findAll() {
		return selectArtiest("SELECT * FROM artiesten");
	}

	// Find by naam
	public Artiest findByNaam(String name) {
		String query = "SELECT * FROM artiesten WHERE naam = '" + name + "';";
		System.out.println("ArtiestDAO: Find by naam Query: " + query);
		return selectArtiest(query).get(0);
	}

	public boolean delete(Artiest artiest) {
		boolean result = false;
		boolean artiestExists = findByNaam(artiest.getNaam()) != null;

		if (artiestExists) {
			String query = "DELETE FROM artiesten WHERE naam = '" + artiest.getNaam() + "';";
			System.out.println("Artiest Delete Query: " + query);
			try (Connection con = getConnection()) {

				Statement stmt = con.createStatement();
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol gedelete
					result = true;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return result;
	}
}
