package ipass.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ipass.model.Album;
import ipass.model.Bibliotheek;

public class AlbumDAO extends BaseDAO{
	
	// selectAlbum functie
	public ArrayList<Album> selectAlbum(String query) {
		ArrayList<Album> results = new ArrayList<Album>();

		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				String naam = dbResultSet.getString("albumNaam");
					
				String genre = dbResultSet.getString("genre");
				String artiest = dbResultSet.getString("artiestNaam");
				String uitvoerende_artiest = dbResultSet.getString("uitvoerende_artiest");
				String type = dbResultSet.getString("type");
				String coverLink = dbResultSet.getString("coverLink");
				int waardering = dbResultSet.getInt("waardering");

				results.add(new Album(naam, genre, artiest, uitvoerende_artiest, type, coverLink, waardering));
				System.out.println("selectAlbum Result added: " + naam);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	// Save statement (Save Album in Database)
	public void Save(Album album) {
		String query = "INSERT INTO albums(albumNaam, artiestNaam, genre, uitvoerende_artiest, type, coverLink, waardering) VALUES ('" + album.getNaam() + "', '" + album.getGenre()  + "', '" + album.getArtiest()  + "', '" + album.getUitvoerende_artiest()  + "', '" + album.getType() + "', '" + album.getCoverLink() + "', '" + album.getWaardering() + "');";
		System.out.println("Save query: " + query);
		
		try (Connection con = getConnection()) {
			Statement stmt = con.createStatement();
			if (stmt.executeUpdate(query) == 1) { // 1 row updated!
				// Succesvol geupdate
				System.out.println("Succesvol gesaved");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
	}

	// Find all()
	public ArrayList<Album> findAll() {
		return selectAlbum("SELECT * FROM albums");
	}

	// Find by naam
	public Album findByNaam(String name) {
		String query = "SELECT * FROM albums WHERE albumNaam = '" + name + "';";
		System.out.println("Find by naam Query: " + query);
		return selectAlbum(query).get(0);
	}

	public boolean delete(Album album) {
		boolean result = false;
		boolean artiestExists = findByNaam(album.getNaam()) != null;

		if (artiestExists) {
			String query = "DELETE FROM albums WHERE albumNaam = '" + album.getNaam() + "';";
			String query2 = "DELETE FROM bibliotheekalbums WHERE albumNaam = '" + album.getNaam() + "';";

			System.out.println("Album Delete Query: " + query);
			try (Connection con = getConnection()) {

				Statement stmt = con.createStatement();
				stmt.executeUpdate(query2);
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol gedelete
					result = true;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return result;
	}
	
	public ArrayList<Album> getAlbumsVanBibliotheek(Bibliotheek bieb) {
		String query = "SELECT a.* FROM albums a, bibliotheekalbums b WHERE b.bibliotheekNaam = '" + bieb.getNaam() + "' AND a.albumNaam = b.albumNaam" + ";";
		System.out.println("Find by naam Query: " + query);
		
		ArrayList<Album> results = new ArrayList<Album>();

		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				String naam = dbResultSet.getString("albumNaam");
				String genre = dbResultSet.getString("genre");
				String artiest = dbResultSet.getString("artiestNaam");
				String uitvoerende_artiest = dbResultSet.getString("uitvoerende_artiest");
				String type = dbResultSet.getString("type");
				String coverLink = dbResultSet.getString("coverLink");
				int waardering = dbResultSet.getInt("waardering");

				results.add(new Album(naam, genre, artiest, uitvoerende_artiest, type, coverLink, waardering));
				System.out.println("selectAlbum Result added: " + naam);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}
}