package ipass.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import ipass.model.User;

public class UserDAO extends BaseDAO {

	// SelectUser functie
	private ArrayList<User> selectUser(String query) {
		ArrayList<User> results = new ArrayList<User>();

		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				String username = dbResultSet.getString("gebruikersnaam");
				String password = dbResultSet.getString("wachtwoord");
				String email = dbResultSet.getString("email");
				String name = dbResultSet.getString("naam");


				results.add(new User(username, password, email, name));
				System.out.println("SelectUser Result added: " + username);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	// Save statement (Save country in Database)
	public void Save(User user) {
		String query = "INSERT INTO gebruikers(gebruikersnaam, wachtwoord, email, naam) VALUES ('" + user.getUsername() + "', '" + user.getPassword() + "', '" + user.getEmail() + "', '" + user.getName() + "');";
		System.out.println(query);
		
		try (Connection con = getConnection()) {
			Statement stmt = con.createStatement();
			if (stmt.executeUpdate(query) == 1) { // 1 row updated!
				// Succesvol geupdate
				System.out.println("Succesvol gesaved");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
	}

	// Find all()
	public ArrayList<User> findAll() {
		return selectUser("SELECT * FROM gebruikers");
	}

	// Find by naam
	public User findByNaam(String name) {
		return selectUser("SELECT username, name FROM gebruikers WHERE naam = " + name).get(0);
	}

	// +update(country : Country) : Country
	public void update(User user) {
		boolean userExists = findByNaam(user.getName()) != null;
		String query = "UPDATE country SET gebruikersnaam=" + user.getUsername() + ",naam="
				+ user.getName() + ", email = " + user.getEmail();

		if (userExists) {
			try (Connection con = getConnection()) {
				Statement stmt = con.createStatement();
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol geupdate
					System.out.println("Succesvol geupdate");
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
	}

	// +delete(country : Country) : boolean
	public boolean delete(User user) {
		boolean result = false;
		boolean countryExists = findByNaam(user.getName()) != null;

		if (countryExists) {
			String query = "DELETE * FROM gebruikers WHERE naam = " + user.getName();

			try (Connection con = getConnection()) {

				Statement stmt = con.createStatement();
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol gedelete
					result = true;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return result;
	}
}