package ipass.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ipass.model.Genre;

public class GenreDAO extends BaseDAO{
	
	// selectGenre functie
	private ArrayList<Genre> selectGenre(String query) {
		ArrayList<Genre> results = new ArrayList<Genre>();

		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				String naam = dbResultSet.getString("naam");


				results.add(new Genre(naam));
				System.out.println("selectGenre Result added: " + naam);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	// Save statement (Save Genre in Database)
	public void Save(Genre genre) {
		String query = "INSERT INTO genres(naam) VALUES ('" + genre.getNaam() + "');";
		System.out.println(query);
		
		try (Connection con = getConnection()) {
			Statement stmt = con.createStatement();
			if (stmt.executeUpdate(query) == 1) { // 1 row updated!
				// Succesvol geupdate
				System.out.println("Succesvol gesaved");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
	}

	// Find all()
	public ArrayList<Genre> findAll() {
		return selectGenre("SELECT * FROM genres");
	}

	// Find by naam
	public Genre findByNaam(String name) {
		String query = "SELECT * FROM genres WHERE naam = '" + name + "';";
		System.out.println("Find by naam Query: " + query);
		return selectGenre(query).get(0);
	}

	public boolean delete(Genre genre) {
		boolean result = false;
		boolean genreExists = findByNaam(genre.getNaam()) != null;

		if (genreExists) {
			String query = "DELETE FROM genres WHERE naam = '" + genre.getNaam() + "';";
			System.out.println("Artiest Delete Query: " + query);
			try (Connection con = getConnection()) {

				Statement stmt = con.createStatement();
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol gedelete
					result = true;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return result;
	}
}
