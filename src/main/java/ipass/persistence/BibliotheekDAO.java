package ipass.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ipass.model.Album;
import ipass.model.Bibliotheek;

public class BibliotheekDAO extends BaseDAO{
	
	// selectBibliotheek functie
	private ArrayList<Bibliotheek> selectBibliotheek(String query) {
		ArrayList<Bibliotheek> results = new ArrayList<Bibliotheek>();

		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				String naam = dbResultSet.getString("bibliotheekNaam");


				results.add(new Bibliotheek(naam));
				System.out.println("selectBibliotheek Result added: " + naam);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	// Save statement (Save Genre in Database)
	public boolean Save(Bibliotheek bieb) {
		String query = "INSERT INTO bibliotheken(bibliotheekNaam) VALUES ('" + bieb.getNaam() + "');";
		System.out.println(query);
		
		try (Connection con = getConnection()) {
			Statement stmt = con.createStatement();
			if (stmt.executeUpdate(query) == 1) { // 1 row updated!
				// Succesvol geupdate
				System.out.println("Succesvol gesaved");
				return true;
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
		return false;
		
	}

	// Find all()
	public ArrayList<Bibliotheek> findAll() {
		return selectBibliotheek("SELECT * FROM bibliotheken");
	}

	// Find by naam
	public Bibliotheek findByNaam(String name) {
		String query = "SELECT * FROM bibliotheken WHERE bibliotheekNaam = '" + name + "';";
		System.out.println("Find by naam Query: " + query);
		return selectBibliotheek(query).get(0);
	}

	public boolean delete(Bibliotheek bieb) {
		boolean result = false;
		boolean biebExists = findByNaam(bieb.getNaam()) != null;

		if (biebExists) {
			String query = "DELETE FROM bibliotheken WHERE bibliotheekNaam = '" + bieb.getNaam() + "';";
			System.out.println("bibliotheken Delete Query: " + query);
			try (Connection con = getConnection()) {

				Statement stmt = con.createStatement();
				if (stmt.executeUpdate(query) == 1) { // 1 row updated!
					// Succesvol gedelete
					result = true;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return result;
	}
	
	public void saveAlbumInBibliotheek(Album a, Bibliotheek b) {
		String query = "INSERT INTO bibliotheekalbums(bibliotheekNaam, albumNaam) VALUES ('" + b.getNaam() + "', '" + a.getNaam() + "');";
		System.out.println(query);
		
		try (Connection con = getConnection()) {
			Statement stmt = con.createStatement();
			if (stmt.executeUpdate(query) == 1) { // 1 row updated!
				// Succesvol geupdate
				System.out.println("Succesvol gesaved");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
	}
	
}
