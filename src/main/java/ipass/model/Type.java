package ipass.model;

public class Type {
	private String naam;
	private String drager;
	
	public Type(String nm, String eig, String fm) {
		naam = nm;
		drager = fm;
	}
	
	public String getNaam() {
		return naam;
	}
	
	public String getDrager() {
		return drager;
	}
	
	public void setNaam(String nm) {
		naam = nm;
	}
	
	public void setDrager(String dr) {
		drager = dr;
	}
}
