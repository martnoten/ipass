package ipass.model;

public class Album {
	private String naam;
	private String genre;
	private String artiest;
	private String uitvoerende_artiest;
	private String type;
	private String coverLink;
	private int waardering;
	
	public Album(String nm, String gn, String at, String uAt, String typ, String cL, int wd) {
		naam = nm;
		genre = gn;
		artiest = at;
		uitvoerende_artiest = uAt;
		type = typ;
		coverLink = cL;
		waardering = wd;
	}
	
	public Album(String n) {
		naam = n;
	}

	public String getNaam() {
		return naam;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public void setCoverLink(String link) {
		coverLink = link;
	}
	
	public String getCoverLink(){
		return coverLink;
	}
	
	public int getWaardering() {
		return waardering;
	}
	
	public String getArtiest() {
		return artiest;
	}
	
	public String getUitvoerende_artiest() {
		return uitvoerende_artiest;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String typ) {
		type = typ;
	}
}
