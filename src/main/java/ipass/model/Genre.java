package ipass.model;

public class Genre {
	private String naam;
	
	public Genre(String nm) {
		naam = nm;
	}
	
	public String getNaam() {
		return naam;
	}
	
}
