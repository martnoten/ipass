package ipass.model;

import java.util.ArrayList;

public class Bibliotheek {
	private String naam;
	private ArrayList<Album> albums = new ArrayList<Album>();
	
	public Bibliotheek(String nm) {
		naam = nm;
		@SuppressWarnings("unused")
		ArrayList<Album> albums = new ArrayList<Album>();
	}
	
	public String getNaam() {
		return naam;
	}
	
	public void addAlbum(Album a) {
		albums.add(a);
	}
	
	public ArrayList<Album> getAlbums() {
		return this.albums;
	}
}
