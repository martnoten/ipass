package ipass.model;

public class Artiest {
	private String naam;
	private String instrument;
	
	public Artiest(String nm, String inst) {
		naam = nm;
		instrument = inst;
	}
	
	public String getNaam() {
		return naam;
	}
	
	public String getInstrument() {
		return instrument;
	}
	
	public void setNaam(String nm){
		naam = nm;
	}
	
	public void setInstrument(String i) {
		instrument = i;
	}
}
