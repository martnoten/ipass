package ipass.model;

public class User {
	

	private String username;
	private String password;
	private String email;
	private String name;

	public User(String uNm, String pw, String em, String nm) {
		username = uNm;
		password = pw;
		email = em;
		name = nm;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	public boolean checkPassword(String pw){
		if (password.equals(pw)) {
			return true;
		} 
			
		return false;
	}
	
	public String getName(){
		return name;
	}
	
	public String getEmail(){
		return email;
	}
	
	public String toString() {
		return name;
	}

}