package ipass.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.services.ServiceProvider;

public class ArtiestVerwijderenServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

			throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		String naam = req.getParameter("naam");


		// Controleer of alles is ingevuld
		if (naam != null) {
				succes = true;
		} else {
			req.setAttribute("msgs", "De naam is leeg");
		}

		// RequestDisPatcher
		RequestDispatcher rd = null;
		if (succes) {			
				if (ServiceProvider.getLibService().artiestVerwijderenOpNaam(naam)) {
				req.setAttribute("msgs", naam + " is succesvol verwijderd.");
				rd = req.getRequestDispatcher("user/artiestToevoegen.jsp");
		
				}
				else {
					req.setAttribute("msgs", "Je hebt nog een album opgeslagen van: " + naam);
					rd = req.getRequestDispatcher("user/artiestToevoegen.jsp");
				}
		} else {
			req.setAttribute("msgs", naam + " ging fout");
			rd = req.getRequestDispatcher("user/artiestToevoegen.jsp");
		}
		
		rd.forward(req, resp);

	}

}