package ipass.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.model.Album;
import ipass.model.Bibliotheek;
import ipass.services.ServiceProvider;

public class AlbumToevoegenAanBibliotheekServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;


	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

			throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		String albumNaam = req.getParameter("albumNaam");
		String bibliotheek = req.getParameter("bibliotheekNaam");
		Album a = ServiceProvider.getLibService().findAlbumByNaam(albumNaam);
		Bibliotheek b = ServiceProvider.getLibService().findBibliotheekByNaam(bibliotheek);
		
		// Controleer of alles is ingevuld
		if (albumNaam != null && bibliotheek != null) {
				succes = true;
		} else {
			req.setAttribute("msgs", "Een van de velden is nog leeg");
		}

		// RequestDisPatcher
		RequestDispatcher rd = null;
		if (succes) {
			if (bibliotheek.isEmpty()) {
			ServiceProvider.getLibService().addAlbumInBibliotheek(a, b);
			
			// Attributes bijweken
			req.setAttribute("msgs", "Het volgende album is toegevoegd: " + albumNaam + " in: " + b.getNaam());
			
			rd = req.getRequestDispatcher("user/albumToevoegenAanBibliotheek.jsp");
			} else {
				ServiceProvider.getLibService().addAlbumInBibliotheek(a, b);

				// Attributes bijweken
				req.setAttribute("msgs", "Het volgende album is toegevoegd: " + albumNaam + " aan " + b.getNaam());
				
				rd = req.getRequestDispatcher("user/albumToevoegenAanBibliotheek.jsp");
			}
		} else {
			rd = req.getRequestDispatcher("user/albumToevoegenAanBibliotheek.jsp");
		}
		rd.forward(req, resp);

	}

}