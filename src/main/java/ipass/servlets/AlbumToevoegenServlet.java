package ipass.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.model.Album;
import ipass.model.Bibliotheek;
import ipass.services.ServiceProvider;

public class AlbumToevoegenServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;


	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

			throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		String albumNaam = req.getParameter("albumNaam");
		String artiestNaam = req.getParameter("artiestNaam");
		String genre = req.getParameter("genre");
		String uitvoerende_artiest = req.getParameter("uitvoerende_artiest");
		String type = req.getParameter("type");
		String bibliotheek = req.getParameter("bibliotheekNaam");
		String coverLink = req.getParameter("coverLink");
		int waardering = Integer.parseInt(req.getParameter("waardering"));
		Bibliotheek b = ServiceProvider.getLibService().findBibliotheekByNaam(bibliotheek);
		
		// Controleer of alles is ingevuld
		if (albumNaam != null && artiestNaam != null && genre != null && type != null) {
				succes = true;
		} else {
			req.setAttribute("msgs", "Een van de velden is nog leeg");
		}

		// RequestDisPatcher
		RequestDispatcher rd = null;
		if (succes) {
			if (bibliotheek.isEmpty()) {
			ServiceProvider.getLibService().albumToevoegen(new Album(albumNaam, artiestNaam, genre, uitvoerende_artiest, type, coverLink, waardering));
			
			// Attributes bijweken
			req.setAttribute("msgs", "Het volgende album is toegevoegd: " + albumNaam + " door: " + artiestNaam);
			
			rd = req.getRequestDispatcher("user/albumToevoegen.jsp");
			} else {
				ServiceProvider.getLibService().albumToevoegen(new Album(albumNaam, artiestNaam, genre, uitvoerende_artiest, type, coverLink, waardering));
				ServiceProvider.getLibService().addAlbumInBibliotheek(new Album(albumNaam, artiestNaam, genre, uitvoerende_artiest, type, coverLink, waardering), b);

				// Attributes bijweken
				req.setAttribute("msgs", "Het volgende album is toegevoegd: " + albumNaam + " door: " + artiestNaam);
				
				rd = req.getRequestDispatcher("user/albumToevoegen.jsp");
			}
		} else {
			rd = req.getRequestDispatcher("user/albumToevoegen.jsp");
		}
		rd.forward(req, resp);

	}

}