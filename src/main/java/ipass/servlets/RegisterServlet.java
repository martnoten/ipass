package ipass.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.services.ServiceProvider;

public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;


	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

			throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean loginSuccess = false;
		String username = req.getParameter("username");
		String pass = req.getParameter("password");
		String pass2x = req.getParameter("password2X");
		String naam = req.getParameter("naam");
		String email = req.getParameter("email");

		// Controleer of alles is ingevuld
		if (username != null && pass != null && pass2x != null && naam != null && email != null) {
			if (pass.equals(pass2x)) {
				loginSuccess = true;
			} else {
				req.setAttribute("msgs", "Wachtwoorden komen niet overeen");
			}
		} else {
			req.setAttribute("msgs2", "Een van de velden is nog leeg");
		}

		// RequestDisPatcher --> Doe dingen na registreren --> Nieuwe gebruiker
		RequestDispatcher rd = null;
		if (loginSuccess) {			
			ServiceProvider.getLibService().registerUser(username, pass, email, naam);
			
			// Attributes bijweken
			req.setAttribute("naam", naam);
			req.setAttribute("username", username);
			req.setAttribute("password", pass);
			req.setAttribute("email", email);
			
			rd = req.getRequestDispatcher("welcome.jsp");
		} else {
			rd = req.getRequestDispatcher("register.jsp");
		}
		rd.forward(req, resp);

	}

}