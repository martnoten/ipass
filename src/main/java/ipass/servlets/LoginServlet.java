package ipass.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.model.User;
import ipass.services.ServiceProvider;


public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 102854373239L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

			throws ServletException, IOException {

		// Haal gegevens op
		boolean loginSuccess = false;
		String name = req.getParameter("username");
		String pass = req.getParameter("password");
		
		System.out.println("LoginServlet req.parameters username: " + name + " en wachtwoord: " + pass);
		ArrayList<User> userList = ServiceProvider.getLibService().userList;

		// q check login details
		if (userList != null) {
			System.out.println("userList niet leeg");
			if (name != null && pass != null) {
				for (User gebruiker : userList) {
					System.out.println("bij inloggen userList: " + gebruiker.getUsername());
					if (loginSuccess = (gebruiker.getUsername().equals(name) && gebruiker.checkPassword(pass))) {
						req.setAttribute("email", gebruiker.getEmail());
						req.setAttribute("naam", gebruiker.getName());
						req.setAttribute("username", gebruiker.getUsername());
						req.setAttribute("password", pass);

						// Zet gebruiker object in Session
						req.getSession().setAttribute("loggedUser", gebruiker);

						// Maak cookie
						resp.addCookie(new Cookie("username", name));
						break;
					}
				}

				if (!loginSuccess) {
					req.setAttribute("msgs", "Gebruikersnaam en/of wachtwoord kloppen niet!");
				}
			}
		} else {
			req.setAttribute("msgs", "Er zijn nog helemaal geen gebruikers");
		}

		if (loginSuccess) {
			resp.sendRedirect("user/home.jsp");
		} else {
			RequestDispatcher rd = null;
			rd = req.getRequestDispatcher("index.jsp");
			rd.forward(req, resp);
		}

	}

}