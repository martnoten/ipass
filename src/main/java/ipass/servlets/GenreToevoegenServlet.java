package ipass.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.model.Genre;
import ipass.services.ServiceProvider;

public class GenreToevoegenServlet extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;


	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

			throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		String naam = req.getParameter("genreNaam");


		// Controleer of alles is ingevuld
		if (naam != null) {
				succes = true;
		} else {
			req.setAttribute("msgs", "Een van de velden is nog leeg");
		}

		// RequestDisPatcher --> Doe dingen na registreren --> Nieuwe gebruiker
		RequestDispatcher rd = null;
		if (succes) {			
			ServiceProvider.getLibService().genreToevoegen(new Genre(naam));
			
			// Attributes bijweken
			req.setAttribute("msgs", naam + " is succesvol toegevoegd!");
			
			rd = req.getRequestDispatcher("user/genreToevoegen.jsp");
		} else {
			rd = req.getRequestDispatcher("user/genreToevoegen.jsp");
		}
		rd.forward(req, resp);

	}

}