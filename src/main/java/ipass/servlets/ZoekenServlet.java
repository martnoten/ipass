package ipass.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.model.Album;
import ipass.services.ServiceProvider;

public class ZoekenServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1583435157383137148L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

			throws ServletException, IOException {

		// Haal doorgestuurde gegevens op
		boolean succes = false;
		ArrayList<Album> resultaten = null;
		String bibliotheekNaam = req.getParameter("bibliotheekNaam");
		String artiestNaam = req.getParameter("artiestNaam");
		String genre = req.getParameter("genre");
		System.out.println(bibliotheekNaam + artiestNaam + genre); 

		// Controleer of alles is ingevuld
		if (!"x".equals(bibliotheekNaam) && !"x".equals(artiestNaam) && !"x".equals(genre)) {
			// Alles ingevuld
			succes = true;
			String query = "SELECT a.* FROM albums a, bibliotheekalbums b WHERE a.albumNaam = b.albumNaam AND a.genre = '"
					+ genre + "' AND " + "b.bibliotheekNaam = '" + bibliotheekNaam + "' AND a.artiestNaam = '"
					+ artiestNaam + "';";
			System.out.println("ZoekenServlet Query String: " + query);
			resultaten = ServiceProvider.getLibService().albumDAO.selectAlbum(query);
			req.setAttribute("zoekResultaten", resultaten);

		} else if (!"x".equals(bibliotheekNaam) && !"x".equals(artiestNaam) && "x".equals(genre)) {
			// bibliotheekNaam en ArtiestNaam is ingevuld, Genre is leeg
			succes = true;
			String query = "SELECT a.* FROM albums a, bibliotheekalbums b WHERE a.albumNaam = b.albumNaam AND "
					+ "b.bibliotheekNaam = '" + bibliotheekNaam + "' AND a.artiestNaam = '" + artiestNaam + "';";
			System.out.println("ZoekenServlet Query String: " + query);
			resultaten = ServiceProvider.getLibService().albumDAO.selectAlbum(query);
			req.setAttribute("zoekResultaten", resultaten);

		} else if (!"x".equals(bibliotheekNaam) && "x".equals(artiestNaam) && "x".equals(genre)) {
			// bibliotheekNaam ingevuld, Artiestnaam en Genre is leeg
			succes = true;
			String query = "SELECT a.* FROM albums a, bibliotheekalbums b WHERE a.albumNaam = b.albumNaam AND "
					+ "b.bibliotheekNaam = '" + bibliotheekNaam + "';";
			System.out.println("ZoekenServlet Query String: " + query);
			resultaten = ServiceProvider.getLibService().albumDAO.selectAlbum(query);
			req.setAttribute("zoekResultaten", resultaten);

		} else if ("x".equals(bibliotheekNaam) && "x".equals(artiestNaam) && "x".equals(genre)) {
			// Alles is leeg
			succes = false;

		} else if ("x".equals(bibliotheekNaam) && !"x".equals(artiestNaam) && !"x".equals(genre)) {
			// ArtiestNaam en Genre zijn ingevuld, bibliotheekNaam is leeg
			succes = true;
			String query = "SELECT a.* FROM albums a, bibliotheekalbums b WHERE a.albumNaam = b.albumNaam AND a.genre = '"
					+ genre + "' AND a.artiestNaam = '" + artiestNaam + "';";
			System.out.println("ZoekenServlet Query String: " + query);
			resultaten = ServiceProvider.getLibService().albumDAO.selectAlbum(query);
			req.setAttribute("zoekResultaten", resultaten);

		} else if ("x".equals(bibliotheekNaam) && "x".equals(artiestNaam) && !"x".equals(genre)) {
			// Genre is ingevuld, bibliotheekNaam en Artiestnaam zijn leeg
			succes = true;
			String query = "SELECT a.* FROM albums a, bibliotheekalbums b WHERE a.albumNaam = b.albumNaam AND a.genre = '"
					+ genre + "';";
			System.out.println("ZoekenServlet Query String: " + query);
			resultaten = ServiceProvider.getLibService().albumDAO.selectAlbum(query);
			req.setAttribute("zoekResultaten", resultaten);

		} else if ("x".equals(bibliotheekNaam) && !"x".equals(artiestNaam) && "x".equals(genre)) {
			// Artiestnaam is ingevuld, bibliotheekNaam en Genre zijn leeg
			succes = true;
			System.out.println("Artiestnaam is ingevuld, bibliotheekNaam en Genre zijn leeg");
			String query = "SELECT a.* FROM albums a WHERE a.artiestNaam = '"
					+ artiestNaam + "';";
			System.out.println("ZoekenServlet Query String: " + query);
			resultaten = ServiceProvider.getLibService().albumDAO.selectAlbum(query);
			req.setAttribute("zoekResultaten", resultaten);

		} else if (!"x".equals(bibliotheekNaam) && "x".equals(artiestNaam) && !"x".equals(genre)) {
			// bibliotheekNaam en Genre zijn ingevuld, artiestnaam is leeg
			succes = true;
			String query = "SELECT a.* FROM albums a, bibliotheekalbums b WHERE a.albumNaam = b.albumNaam AND a.genre = '"
					+ genre + "' AND " + "b.bibliotheekNaam = '" + bibliotheekNaam + "';";
			System.out.println("ZoekenServlet Query String: " + query);
			resultaten = ServiceProvider.getLibService().albumDAO.selectAlbum(query);
		}

		// RequestDisPatcher
		RequestDispatcher rd = null;
		if (succes) {
			
			// verwijder dubbele resultaten
			if (!resultaten.isEmpty()) {		
				req.setAttribute("msgs", "De volgende resultaten zijn gevonden");
				req.getSession().setAttribute("zoekResultaten", resultaten);
				resp.sendRedirect("user/zoekResultaten.jsp");
			} else {
				req.setAttribute("msgs", "Er zijn geen resultaten gevonden");
				rd = req.getRequestDispatcher("user/zoeken.jsp");
				rd.forward(req, resp);
			}
		} else {
			req.setAttribute("msgs", "Je hebt niks ingevuld");
			rd = req.getRequestDispatcher("user/zoeken.jsp");
			rd.forward(req, resp);
		}
	}

}