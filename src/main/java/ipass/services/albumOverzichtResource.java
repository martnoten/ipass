package ipass.services;


import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import ipass.model.Album;

@Path("/getAlbumInfo")
public class albumOverzichtResource  {
	
	@GET
	@Path("{naam}")
	@Produces("application/json")
	public String getAlbumInfo(@PathParam("naam") String naam) {
		JsonObjectBuilder job = Json.createObjectBuilder();
		String albumNaam = naam.replace("%20", " ");
		System.out.println("AlbumoverzichtResource albumNaam: " + albumNaam);
		Album a = ServiceProvider.getLibService().findAlbumByNaam(albumNaam);
		job.add("albumNaam",  a.getNaam());
		job.add("artiestNaam",  a.getArtiest());
		job.add("type",  a.getType());
		job.add("genre", a.getGenre());
		job.add("waardering", a.getWaardering());
		if (a.getCoverLink() != null) {
			job.add("coverLink", a.getCoverLink());
		}

		if (a != null) {
			System.out.println("AlbumOverzichtResource: A is niet null");
			return job.build().toString();
		} 
	
		return job.build().toString();

	}

}