package ipass.services;

import java.util.ArrayList;
import java.util.List;

import ipass.persistence.UserDAO;
import ipass.model.User;

public class UserService {
	private UserDAO userDAO = new UserDAO();
	
	public List<User> getAllUsers() {
		return userDAO.findAll();
	}
	
	
	public User getUserByName(String naam) {
		User result = null;
		
		List<User> alleCountries = new ArrayList<>();
		alleCountries = userDAO.findAll();
		
		for (User c : alleCountries) {
			System.out.println(c);
			if (c.getName().equals(naam)) {
				result = c;
				break;
			}
		}
		
		return result;
	}
}
