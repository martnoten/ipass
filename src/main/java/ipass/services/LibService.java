package ipass.services;

import java.util.ArrayList;
import javax.servlet.http.HttpServlet;

import ipass.model.Album;
import ipass.model.Artiest;
import ipass.model.Bibliotheek;
import ipass.model.Genre;
import ipass.model.User;
import ipass.persistence.AlbumDAO;
import ipass.persistence.ArtiestDAO;
import ipass.persistence.BibliotheekDAO;
import ipass.persistence.GenreDAO;
import ipass.persistence.UserDAO;

public class LibService extends HttpServlet {
	private static final long serialVersionUID = 102432343239L;
	private UserDAO userDAO = new UserDAO();
	private ArtiestDAO artiestDAO = new ArtiestDAO();	
	public AlbumDAO albumDAO = new AlbumDAO();	
	private GenreDAO genreDAO = new GenreDAO();
	private BibliotheekDAO bibliotheekDAO = new BibliotheekDAO();
	public ArrayList<User> userList = userDAO.findAll();
	public ArrayList<Artiest> artiestList = artiestDAO.findAll();
	public ArrayList<Genre> genreList = genreDAO.findAll();
	public ArrayList<Bibliotheek> bibliotheekList = bibliotheekDAO.findAll();
	public ArrayList<Album> albumList = albumDAO.findAll();


	public ArrayList<User> getUserList() {
		userList = userDAO.findAll();
		return userList;
	}
	
	public ArrayList<Artiest> getArtiestList() {
		artiestList = artiestDAO.findAll();
		return artiestList;
	}
	
	public ArrayList<Genre> getGenreList() {
		genreList = genreDAO.findAll();
		return genreList;
	}
	
	public ArrayList<Album> getAlbumList() {
		albumList = albumDAO.findAll();
		return albumList;
	}
	
	public ArrayList<Bibliotheek> getBibliotheekList() {
		bibliotheekList = bibliotheekDAO.findAll();
		return bibliotheekList;
	}
	
	public boolean registerUser(String uNm, String pw, String em, String rN) {
		for (User gebruiker : userList) {
			
			if (gebruiker.getName().equals(uNm)) {
				System.out.println("RegisterUser: Gebruiker bestaat al " + gebruiker.getName() + " && " + uNm);
				return false;
			}
		}
		
		// Geen gebruiker met dezelfde naam
		userList.add(new User(uNm, pw, em, rN));
		userDAO.Save(new User(uNm, pw, em, rN));
		return true;	
	}
	
	public User loginUser(String uNm, String pw) {
		for (User gebruiker : userList) {
			if (gebruiker.getName() == uNm && gebruiker.checkPassword(pw)) {
				return gebruiker;
			}
		}
		
		// Geen gebruiker gevonden met dezelfde naam en wachtwoord
		return null;
	}

	public boolean artiestToevoegen(Artiest artiest) {
		ArrayList<Artiest> artiestList = artiestDAO.findAll();
		
		for (Artiest art : artiestList) {
			
			if (art.getNaam().equals(artiest.getNaam())) {
				System.out.println("ArtiestToevoegen: Artiest bestaat al " + art.getNaam() + " && " + artiest.getNaam());
				return false;
			}
		}
		
		artiestDAO.Save(new Artiest(artiest.getNaam(), artiest.getInstrument()));
		return true;	
	}

	public boolean artiestVerwijderenOpNaam(String naam) {
		Artiest artiest = artiestDAO.findByNaam(naam);
		if (artiest.getInstrument().isEmpty()) {
			artiest.setInstrument("");
		}
		if(artiestDAO.delete(new Artiest(naam, artiest.getInstrument()))) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean albumToevoegen(Album album) {
		ArrayList<Album> albumList = albumDAO.findAll();
		
		for (Album alb : albumList) {	
			if (alb.getNaam().equals(album.getNaam())) {
				System.out.println("ArtiestToevoegen: Artiest bestaat al " + alb.getNaam() + " && " + album.getNaam());
				return false;
			}
		}
		
		albumDAO.Save(new Album(album.getNaam(), album.getGenre(), album.getArtiest(), album.getUitvoerende_artiest(), album.getType(), album.getCoverLink(), album.getWaardering()));
		return true;	
	}
	
	public boolean albumVerwijderenOpNaam(String naam) {
		Album album = albumDAO.findByNaam(naam);
		
		albumDAO.delete(new Album(album.getNaam(), album.getGenre(), album.getArtiest(), album.getUitvoerende_artiest(), album.getType(), album.getCoverLink(), album.getWaardering()));
		return true;	
	}
	
	public boolean genreToevoegen(Genre genre) {
		genreList = genreDAO.findAll();
		
		for (Genre gen : genreList) {	
			if (gen.getNaam().equals(genre.getNaam())) {
				System.out.println("ArtiestToevoegen: Artiest bestaat al " + gen.getNaam() + " && " + genre.getNaam());
				return false;
			}
		}
		
		genreDAO.Save(new Genre(genre.getNaam()));
		return true;	
	}

	public boolean genreVerwijderenOpNaam(String naam) {

		Genre genre = genreDAO.findByNaam(naam);
		
		if (genreDAO.delete(new Genre(genre.getNaam()))) {
			return true;	
		} else {
			return false;
		}
	}
	
	public boolean bibliotheekVerwijderenOpNaam(String naam) {

		Bibliotheek bibliotheek = bibliotheekDAO.findByNaam(naam);
		
		if (bibliotheekDAO.delete(new Bibliotheek(bibliotheek.getNaam()))) {
			return true;	
		} else {
			return false;
		}
	}
	
	public boolean bibliotheekToevoegen(Bibliotheek b) {
		bibliotheekList = bibliotheekDAO.findAll();
		
		for (Bibliotheek bieb : bibliotheekList) {	
			if (bieb.getNaam().equals(b.getNaam())) {
				System.out.println("bibliotheekToevoegen: Bibliotheek bestaat al " + bieb.getNaam() + " && " + b.getNaam());
				return false;
			}
		}
		
		if (bibliotheekDAO.Save(b)){
			return true;	
		}
		
		System.out.println("bibliotheekToevoegen: kon " + b.getNaam() + " niet succesvol opslaan via bibliotheekDAO.save()");
		return false;
	}
	
	public void addAlbumInBibliotheek(Album a, Bibliotheek b) {
			bibliotheekDAO.saveAlbumInBibliotheek(a, b);
			System.out.println(a.getNaam() + " is toegevoegd aan bibliotheek: " + b.getNaam());
	}
	
	public ArrayList<Album> getAlbumsVanBibliotheek(Bibliotheek bieb) {
		ArrayList<Album> albumsBieb = albumDAO.getAlbumsVanBibliotheek(bieb);
		
		return albumsBieb;
	}

	public Bibliotheek findBibliotheekByNaam(String n) {
		Bibliotheek b = bibliotheekDAO.findByNaam(n);
		return b;
	}
	
	public Album findAlbumByNaam(String n) {
		Album a = albumDAO.findByNaam(n);
		return a;
	}
}
